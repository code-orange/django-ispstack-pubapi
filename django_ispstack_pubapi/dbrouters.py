class VoipStackRouter:
    """
    A router to control all database operations on models in the
    django_voipstack application.
    """

    def db_for_read(self, model, **hints):
        """
        Attempts to read django_voipstack models go to auth_db.
        """
        if model._meta.app_label == "django_ispstack_pubapi_models":
            return None
        if model._meta.app_label.startswith("django_voipstack"):
            return "voipstack"
        if model._meta.app_label.startswith("django_ispstack"):
            return "ispstack"
        return None

    def db_for_write(self, model, **hints):
        """
        Attempts to write django_voipstack models go to auth_db.
        """
        if model._meta.app_label == "django_ispstack_pubapi_models":
            return None
        if model._meta.app_label.startswith("django_voipstack"):
            return "voipstack"
        if model._meta.app_label.startswith("django_ispstack"):
            return "ispstack"
        return None

    def allow_relation(self, obj1, obj2, **hints):
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        return None
