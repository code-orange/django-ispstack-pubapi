from django.contrib import admin
from django.urls import path, include

from django_voipstack_cdr_api.django_voipstack_cdr_api import urls as api_urls

urlpatterns = [
    path("admin/", admin.site.urls),
    path("api/", include(api_urls)),
]
